/*
 * @file
 * JavaScript for conditional_refresh.
 */

(function($) {

  Drupal.behaviors.conditionalRefresh = {
    attach: function() {
      if (Drupal.settings.conditional_refresh == null) {
        return;
      }
      var conditionalRefresh = Drupal.settings.conditional_refresh;

      var settings = conditionalRefresh.settings;
      // Interval is stored in seconds.
      var interval = parseInt(settings.interval) * 1000;
      var forceRefresh = settings.refresh;
      var message = settings.message;

      var currentPath = conditionalRefresh.current_path;

      // This variable is used to compare the refresh value.
      var refreshCheckValue = false;

      var requestData = {};
      requestData['path'] = currentPath;

      setTimeout(refreshCallback, interval, requestData);

      function refreshCallback(requestData) {
        $.ajax({
          url : '/conditional-refresh',
          method: 'GET',
          data: requestData
        }).done(
            function (handleData) {
              var responseValue = handleData.refresh_response_value;
              if (refreshCheckValue === false) {
                refreshCheckValue = responseValue;
                setTimeout(refreshCallback, interval, requestData);
                return;
              }

              if (responseValue != refreshCheckValue) {
                handleRefresh(forceRefresh, message);
              }
              else {
                setTimeout(refreshCallback, interval, requestData);
              }
            }
        );
      }

      function handleRefresh(forceRefresh, message) {
        if (forceRefresh) {
          location.reload();
        }

        if (message && $('#conditional-refresh-message').length == 0) {
          var refreshLink = 'javascript:history.go(0);';
          var messageWithLink = message.replace('@refreshlink', refreshLink);
          var $message = $('<div id="conditional-refresh-message">' + messageWithLink + '</div>');
          $('body').append($message);
        }
      }
    }
  };
})(jQuery);
